package test.io.itit.poi;

import java.util.Date;

import io.itit.poi.excel.ExcelCell;

/**
 * 
 * @author skydu
 *
 */
public class User {

    @ExcelCell(name = "姓名")
    public String name;

    @ExcelCell(name = "年龄")
    public int age;
    
    @ExcelCell(name = "性别")
    public boolean isMale;
    
    @ExcelCell(name = "经度")
    public double longitude;
    
    @ExcelCell(name = "维度")
    public double latitude;
    
    @ExcelCell(name = "联系电话")
    public String phone;

    @ExcelCell(name = "地址")
    public String address;
    
    @ExcelCell(name = "创建时间")
    public Date createTime;

	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + ", isMale=" + isMale + ", longitude=" + longitude + ", latitude="
				+ latitude + ", phone=" + phone + ", address=" + address + ", createTime=" + createTime + "]";
	}
    
    
}
