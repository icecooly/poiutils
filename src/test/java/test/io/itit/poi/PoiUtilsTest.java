package test.io.itit.poi;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import io.itit.poi.PoiUtils;
import io.itit.poi.excel.TableData;

/**
 * 
 * @author skydu
 *
 */
public class PoiUtilsTest {

	@Test
	public void testReadExcelFromFile() throws Exception{
		List<User> users=PoiUtils.readExcel(User.class,new File("/tmp/user.xlsx"));
		for (User user : users) {
			System.out.println(user);
		}
	}
	
	@Test
	public void testReadExcel() throws Exception{
		InputStream is=this.getClass().getResourceAsStream("user.xlsx");
		if(is!=null){
			List<User> users=PoiUtils.readExcel(User.class,is);
			for (User user : users) {
				System.out.println(user);
			}
		}
	}
	
	@Test
	public void testReadExcelAsTableData() throws Exception{
		InputStream is=this.getClass().getResourceAsStream("user.xlsx");
		if(is!=null){
			TableData data=PoiUtils.readExcel(is);
			System.out.println(data);
		}
		
	}
	
	@Test
	public void testAddPicture() throws Exception{
		byte[] image=Files.readAllBytes(Paths.get("/tmp/logo.jpg"));
		PoiUtils.addPicture(new File("/tmp/user.xlsx"),
				image,
				2, 15, 12, 30);
	}
	
	@Test
	public void testWriteExcelToFile() throws Exception{
		List<User> users=new ArrayList<>();
		for(int i=0;i<10;i++){
			User user=new User();
			user.name="测试"+i;
			user.latitude=22.53383;
			user.longitude=113.9422;
			user.age=20;
			user.isMale=true;
			user.phone="1311111111";
			user.createTime=new Date();
			users.add(user);
		}
		PoiUtils.writeExcelToFile(users, new File("/tmp/user.xlsx"));
	}
	
	@Test
	public void testWriteMoreSheetExcelToFile() throws Exception{
		List<User> users=new ArrayList<>();
		for(int i=0;i<10;i++){
			User user=new User();
			user.name="测试"+i;
			user.latitude=22.53383;
			user.longitude=113.9422;
			user.age=20;
			user.isMale=true;
			user.phone="1311111111";
			user.createTime=new Date();
			users.add(user);
		}
		PoiUtils.writeMoreSheetExcelToFile("用户1",users,new File("/tmp/user.xlsx"),5,10);
		PoiUtils.writeMoreSheetExcelToFile("用户2",users,new File("/tmp/user.xlsx"));
	}
	
	
}
