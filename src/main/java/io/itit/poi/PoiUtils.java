package io.itit.poi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.itit.poi.excel.ExcelUtils;
import io.itit.poi.excel.TableData;

/**
 * 
 * @author skydu
 *
 */
public class PoiUtils {
	//
	/**
	 * 
	 * @param wb
	 * @param sheetName
	 * @param list
	 * @throws Exception
	 */
	public static <T> void writeExcel(XSSFWorkbook wb,String sheetName,List<T> list) throws Exception {
		 ExcelUtils.writeExcel(wb,sheetName, list);
	}
	/**
	 * 
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public static <T> ByteArrayOutputStream writeExcel(List<T> list) throws Exception {
		return writeExcel(null, list);
	}

	/**
	 * 
	 * @param list
	 * @param file
	 * @throws Exception
	 */
	public static <T> void writeExcelToFile(List<T> list, File file) throws Exception {
		writeExcelToFile(null, list, file);
	}

	/**
	 * 
	 * @param list
	 * @param file
	 * @throws Exception
	 */
	public static <T> void writeExcelToFile(String sheetName, List<T> list, File file) throws Exception {
		ExcelUtils.writeExcelToFile(sheetName, list, file);
	}

	/**
	 *
	 * @param sheetName
	 * @param list
	 * @param file
	 * @param <T>
	 * @throws Exception
	 */
	public static <T> void writeMoreSheetExcelToFile(String sheetName, List<T> list, File file) throws Exception {
		ExcelUtils.writeMoreSheetExcelToFile(sheetName, list, file,0,0);
	}
	
	public static <T> void writeMoreSheetExcelToFile(String sheetName, List<T> list, File file,int startRow,int startCol) throws Exception {
		ExcelUtils.writeMoreSheetExcelToFile(sheetName,list,file,startRow,startCol);
	}

	/**
	 * 
	 * @param sheetName
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public static <T> ByteArrayOutputStream writeExcel(String sheetName, List<T> list) throws Exception {
		return ExcelUtils.writeExcel(sheetName, list);
	}

	/**
	 * 
	 * @param data
	 * @return
	 * @throws IOException
	 */
	public static ByteArrayOutputStream writeExcel(TableData data) throws IOException {
		return ExcelUtils.writeExcel(data);
	}
	
	/**
	 * 
	 * @param xlsxFile
	 * @param imageData
	 * @param col1
	 * @param row1
	 * @param col2
	 * @param row2
	 * @throws IOException
	 */
	public static void addPicture(File xlsxFile,byte[] imageData,
			 int col1, int row1, int col2, int row2) throws IOException{
		ExcelUtils.addPicture(xlsxFile,imageData,col1, row1, col2, row2);
	}

	/**
	 * 
	 * @param clazz
	 * @param in
	 * @return
	 * @throws Exception
	 */
	public static <T> List<T> readExcel(Class<T> clazz, InputStream in) throws Exception {
		return ExcelUtils.readExcel(clazz, in);
	}
	
	/**
	 * 
	 * @param clazz
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static <T> List<T> readExcel(Class<T> clazz, File file) throws Exception {
		return ExcelUtils.readExcel(clazz, new FileInputStream(file));
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws Exception
	 */
	public static TableData readExcel(InputStream in) throws Exception {
		return ExcelUtils.readExcel(in);
	}

}
