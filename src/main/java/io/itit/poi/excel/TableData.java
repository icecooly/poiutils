package io.itit.poi.excel;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author skydu
 *
 */
public class TableData {

	public String sheetName;
	
	public List<String>headers;
	
	public List<List<String>>contents;
	//
	public TableData() {
		headers=new ArrayList<String>();
		contents=new ArrayList<List<String>>();
	}
	//
	@Override
	public String toString() {
		return "TableData [sheetName=" + sheetName + ", headers=" + headers + ", contents=" + contents + "]";
	}
	
}
