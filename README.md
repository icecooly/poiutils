# PoiUtils
封装Poi

======

Usage
==============
1.从Excel文件中读取内容
```java

import java.util.Date;
import io.itit.poi.excel.ExcelCell;

public class User {
    @ExcelCell(name = "姓名")
    public String name;

    @ExcelCell(name = "年龄")
    public int age;
    
    @ExcelCell(name = "性别")
    public boolean isMale;
    
    @ExcelCell(name = "经度")
    public double longitude;
    
    @ExcelCell(name = "维度")
    public double latitude;
    
    @ExcelCell(name = "联系电话")
    public String phone;

    @ExcelCell(name = "地址")
    public String address;
    
    @ExcelCell(name = "创建时间")
    public Date createTime;
}
//
InputStream is=this.getClass().getResourceAsStream("user.xlsx");		
List<User> users=PoiUtils.readExcel(User.class,is);

```
		
2.把对象列表写入Excel
```java
List<User> users=new ArrayList<>();
for(int i=0;i<10;i++){
	User user=new User();
	user.name="测试"+i;
	user.latitude=22.53383;
	user.longitude=113.9422;
	user.age=20;
	user.isMale=true;
	user.phone="1311111111";
	user.createTime=new Date();
	users.add(user);
}
PoiUtils.writeExcelToFile(users, new File("/tmp/user.xlsx"))

```
截图如下：
![](https://git.oschina.net/icecooly/poiutils/raw/master/src/test/java/test/io/itit/poi/users.png)

3.往Excel文件里添加图片
```java
@Test
public void testAddPicture() throws Exception{
	byte[] image=Files.readAllBytes(Paths.get("/tmp/logo.jpg"));
	PoiUtils.addPicture(new File("/tmp/user.xlsx"),
				image,
				2, 15, 12, 30);
}
```
